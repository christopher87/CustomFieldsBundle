<?php

namespace Melodies\CustomFieldsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FieldRepository extends EntityRepository
{
	public function getSubFieldsByPattern($id_field)
	{
		$qb = $this->_em->createQueryBuilder();

		$qb
			->select('f')
			->from($this->_entityName, 'f')
			->where('f.pattern != 0')
			->andWhere('f.parent = :id_parent')
			->setParameter('id_parent', $id_field)
		;

		$result = $qb->getQuery()->getArrayResult();

		$pattern = [];

		$allFields = $this->getFields();

		if($result)
		{
			foreach($result as $field)
			{
				// override type and name field with pattern
				$field['name']       = $allFields[ $field['type'] ]['name'];
				$field['parentSlug'] = $field['type'];
				$field['type']       = $allFields[ $field['type'] ]['type'];

				$pattern[$field['pattern']][] = $field;
			}
		}

		return $pattern;
	}

	public function getFields()
	{
		$qb = $this->_em->createQueryBuilder();

		$qb
			->select('f.slug, f.type, f.name')
			->from($this->_entityName, 'f')
		;

		$typeFields = [];

		$result = $qb->getQuery()->getArrayResult();

		foreach($result as $field)
		{
			$typeFields[$field['slug']] = [
				'type' => $field['type'],
				'name' => $field['name']
			];
		}

		return $typeFields;
	}
}