<?php

/**
 * Created by PhpStorm.
 * User: guillaume
 */
namespace Melodies\CustomFieldsBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class FormService {
	protected $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function getListRoutes()
	{
		$router = $this->container->get('router');
		$collectionRoutes = $router->getRouteCollection()->all();

		$routes = [];

		foreach ($collectionRoutes as $route => $params)
		{
			$defaults = $params->getDefaults();

			if (isset($defaults['_controller']))
			{
				$controllerAction = explode(':', $defaults['_controller']);
				$controller = $controllerAction[0];

				if (!isset($routes[$controller])) {
					$routes[$controller] = [];
				}

				if(substr( $route, 0, 1 ) !== "_")
					$routes[$controller][$route]= $route;
			}
		}

		$routes['common'] = [
			"header" => "Header",
			"footer" => "Footer"
		];

		return $routes;
	}
}