<?php

namespace Melodies\CustomFieldsBundle\Model;


abstract class Field
{
	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var bool
	 */
	protected $isNullable;

	/**
	 * @var text
	 */
	protected $content;

	/**
	 * @var Relation
	 */
	protected $page;

	/**
	 * @var int
	 */
	protected $slug;

	/**
	 * @var int
	 */
	protected $position;

	/**
	 * @var int
	 */
	protected $parent = 0;

	/**
	 * @var int
	 */
	protected $pattern = 0;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Field
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Field
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Set isNullable
	 *
	 * @param boolean $isNullable
	 * @return Field
	 */
	public function setIsNullable($isNullable)
	{
		$this->isNullable = $isNullable;

		return $this;
	}

	/**
	 * Get isNullable
	 *
	 * @return boolean
	 */
	public function getIsNullable()
	{
		return $this->isNullable;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 * @return Field
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Set page
	 *
	 * @param Page $page
	 * @return Field
	 */
	public function setPage(Page $page = null)
	{
		$this->page = $page;

		return $this;
	}

	/**
	 * Get page
	 *
	 * @return Page
	 */
	public function getPage()
	{
		return $this->page;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 * @return Field
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return Field
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set parent
	 *
	 * @param integer $parent
	 * @return Field
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;

		return $this;
	}

	/**
	 * Get parent
	 *
	 * @return integer
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Set pattern
	 *
	 * @param integer $pattern
	 * @return Field
	 */
	public function setPattern($pattern)
	{
		$this->pattern = $pattern;

		return $this;
	}

	/**
	 * Get pattern
	 *
	 * @return integer
	 */
	public function getPattern()
	{
		return $this->pattern;
	}

	public function setId($id)
	{

	}
}