<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 */

namespace Melodies\CustomFieldsBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

abstract class Page {
	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $idPage;

	/**
	 * @var Relation
	 */
	protected $fields;

	/**
	 * @var string
	 */
	protected $type;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Page
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set idPage
	 *
	 * @param string $idPage
	 * @return Page
	 */
	public function setIdPage($idPage)
	{
		$this->idPage = $idPage;

		return $this;
	}

	/**
	 * Get idPage
	 *
	 * @return string
	 */
	public function getIdPage()
	{
		return $this->idPage;
	}

	/**
	 * Set fields
	 *
	 * @param string $fields
	 * @return Page
	 */
	public function setFields($fields)
	{
		$this->fields = $fields;

		return $this;
	}

	/**
	 * Get fields
	 *
	 * @return string
	 */
	public function getFields()
	{
		return $this->fields;
	}
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->fields = new ArrayCollection();
	}

	/**
	 * Add fields
	 *
	 * @param Field $field
	 * @return Page
	 */
	public function addField(Field $field)
	{
		$field->setPage($this);
		$this->fields->add($field);
	}

	/**
	 * Remove fields
	 *
	 * @param Field $fields
	 */
	public function removeField(Field $fields)
	{
		$this->fields->removeElement($fields);
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Type
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
}