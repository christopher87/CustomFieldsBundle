<?php

namespace Melodies\CustomFieldsBundle\Form;

use Melodies\CustomFieldsBundle\Entity\Field;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class PatternType extends AbstractType
{
	private $fields;


	public function __construct($fields)
	{
		$this->fields = $fields;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		foreach($this->fields as $field){
			$this->getTypeForm($field, $builder);
		}
	}

	private function getTypeForm(Field $field, $form)
	{
		switch($field->getType())
		{
			case 'text':
				$form->add($field->getSlug(), $field->getType(), [
					'label' => $field->getName(),
					'data' => $field->getContent()
				]);
				break;
			case 'textarea':
				$form->add($field->getSlug(), $field->getType(), [
					'label' => $field->getName(),
					'data' => $field->getContent(),
				]);
				break;
			case 'ckeditor':
				$form->add($field->getSlug(), CKEditorType::class, [
					'data' => $field->getContent()
				]);
				break;
			case 'integer':
				$form->add($field->getSlug(), 'integer', [
					'label' => $field->getName()
				]);
				break;
			case 'repeater':
				$collectionForm = $this->generatePattern($field->getId());
				$form->add($field->getSlug(), CollectionType::class, [
					'label'        => $field->getName(),
					'required'     => false,
					'type'         => $collectionForm, //'contact',
					'prototype'    => true,
					'allow_add'    => true,
					'allow_delete' => true,
					'attr'         => [
						'class' => 'repeater_field'
					],
					'options'      => [ ]
				]);
				break;
			case 'image':
				$form->add($field->getSlug(), FileType::class, array(
					'label' => $field->getName(),
					'image_path' => $field->getContent(),
					'required' => false
				));
				break;
			default:
				$form->add($field->getSlug(), $field->getType(), [
					'label' => $field->getName(),
					'data' => $field->getContent()
				]);

		}
	}

	public function getName()
	{
		return 'pattern_type';
	}
}