<?php

namespace Melodies\CustomFieldsBundle\Controller;

use Melodies\CustomFieldsBundle\Entity\Field;
use Melodies\CustomFieldsBundle\Entity\Page;
use Melodies\CustomFieldsBundle\Form\FieldType;
use Melodies\CustomFieldsBundle\Form\PatternType;
use Melodies\CustomFieldsBundle\Form\Type\CustomFieldType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class ManageController extends Controller
{
    public function pageListAction()
    {
        $pages = $this->getDoctrine()->getRepository('MelodiesCustomFieldsBundle:Page')->findAll();

        return $this->render('MelodiesCustomFieldsBundle:Manage:pageList.html.twig', [
            "pages" => $pages
        ]);
    }

    public function pageAddAction(Request $request)
    {
        $page = new Page();
        $formBuilder = $this->createFormBuilder($page);

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        $formBuilder
            ->add('name', 'text', [
                'label'              => 'form.page_name',
                'translation_domain' => 'MCustomFields'
            ])
            ->add('idPage', ChoiceType::class, [
                'label'              => 'form.route_name',
                'translation_domain' => 'MCustomFields',
                'choices'            => $this->get( 'melodies.custom_fields.service.form')->getListRoutes()
            ])
            ->add("save", SubmitType::class, [
                'label' => 'action.add',
                'translation_domain' => 'MCustomFields',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            $page_exist = $this->getDoctrine()->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['idPage' => $page->getIdPage()]);

            if(!$page_exist)
            {
                $em = $this->getDoctrine()->getManager();

                $page->setType('page');

                $em->persist($page);
                $em->flush();

                $this->addFlash('success', 'la page a bien été générée');
            }
            else
            {
                $url_exist = $this->generateUrl('melodies_custom_fields_field_edit', ['id' => $page->getId()]);

                $this->addFlash('danger', 'Un formulaire existe déjà pour cette page <a class="alert-danger" href="'.$url_exist.'"><strong>Voir la page</strong></a>');

                return $this->redirect($this->generateUrl('melodies_custom_fields_page_add'));
            }

            return $this->redirect($this->generateUrl('melodies_custom_fields_page_list'));
        }

        return $this->render('MelodiesCustomFieldsBundle:Manage:pageAdd.html.twig', [
            "form" => $form->createView()
        ]);
    }

    public function blocAddAction(Request $request)
    {
        $page = new Page();
        $formBuilder = $this->createFormBuilder($page);

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        $formBuilder
            ->add('name', 'text', [
                'label'              => 'form.page_name',
                'translation_domain' => 'MCustomFields'
            ])
            ->add('idPage', TextType::class, [
                'label'              => 'form.route_name',
                'translation_domain' => 'MCustomFields'
            ])
            ->add("save", SubmitType::class, [
                'label' => 'action.add',
                'translation_domain' => 'MCustomFields',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            $page_exist = $this->getDoctrine()->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['idPage' => $page->getIdPage()]);

            if(!$page_exist)
            {
                $em = $this->getDoctrine()->getManager();

                $page->setType('bloc');

                $em->persist($page);
                $em->flush();

                $this->addFlash('success', 'la page a bien été générée');
            }
            else
            {
                $url_exist = $this->generateUrl('melodies_custom_fields_field_edit', ['id' => $page->getId()]);

                $this->addFlash('danger', 'Un formulaire existe déjà pour cette page <a class="alert-danger" href="'.$url_exist.'"><strong>Voir la page</strong></a>');

                return $this->redirect($this->generateUrl('melodies_custom_fields_page_add'));
            }

            return $this->redirect($this->generateUrl('melodies_custom_fields_page_list'));
        }

        return $this->render('MelodiesCustomFieldsBundle:Manage:blocAdd.html.twig', [
            "form" => $form->createView()
        ]);
    }

    public function pageEditAction($id, Request $request)
    {
        $page = $this->getDoctrine()->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['id' => $id]);

        $formBuilder = $this->createFormBuilder($page);

        $formBuilder
            ->add('name', 'text', [
                'label'              => 'form.page_name',
                'translation_domain' => 'MCustomFields'
            ])
        ;

        if($page->getType() == "page")
        {
            $formBuilder->add('idPage', ChoiceType::class, [
                'label'              => 'form.route_name',
                'translation_domain' => 'MCustomFields',
                'choices'            => $this->get( 'melodies.custom_fields.service.form')->getListRoutes()
            ]);
        }
        else
        {
            $formBuilder->add('idPage', TextType::class, [
                'label'              => 'form.route_name',
                'translation_domain' => 'MCustomFields',
            ]);
        }

        $formBuilder
            ->add('fields', CustomFieldType::class, [
                'label'              => 'form.fields',
                'translation_domain' => 'MCustomFields',
                'type'               => 'melodies_field',
            ])
            ->add("save", SubmitType::class, [
                'label' => 'action.edit',
                'translation_domain' => 'MCustomFields',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;



        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush();

            $this->addFlash('success', 'Les champs ont bien été modifiés');

            return $this->redirect($this->generateUrl('melodies_custom_fields_page_list'));
        }

        return $this->render('MelodiesCustomFieldsBundle:Manage:pageEdit.html.twig', [
            "page" => $page,
            "form" => $form->createView()
        ]);
    }

    public function fieldEditAction($id, Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $e_field  = $em->getRepository( 'MelodiesCustomFieldsBundle:Field' );
        $e_page   = $em->getRepository( 'MelodiesCustomFieldsBundle:Page' );

        $page = $e_page->findOneBy(['id' => $id]);

        if(!$page)
            throw $this->createNotFoundException(sprintf('Id %s not found', $id));

        $fields = $e_field->findBy(['page' => $page]);

        $formBuilder = $this->createFormBuilder();

        foreach($fields as $field)
        {
            //show field with no parent
            if($field->getParent() == 0 && $field->getPattern() == 0)
                $this->getTypeForm($field, $formBuilder);
        }

        $formBuilder
            ->add("save", SubmitType::class, [
                'label' => 'action.edit',
                'translation_domain' => 'MCustomFields',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            foreach($form as $k => $f)
            {
                if( $f->getName() == "save" ) continue;

                if($f->getConfig()->getType()->getName() == "file")
                {
                    $web_path = $this->get('kernel')->getRootDir() . '/../web/uploads/scf/images/';

                    if(is_object($f->getViewData()))
                    {
                        $newname = uniqid().'.'.$f->getViewData()->getClientOriginalExtension();

                        $f->getViewData()->move( $web_path, $newname);

                        $upField = $e_field->findOneBy(['slug' => $f->getName()]);
                        $upField->setContent($newname);

                        $em->persist($upField);
                    }
                }
                elseif($f->getConfig()->getType()->getName() == "collection")
                {
                    $collectionField = $e_field->findOneBy( [ 'slug' => $f->getName() ] );
                    $idCollection    = $collectionField->getId();
                    $page            = $collectionField->getPage();

                    $a_data = $f->getViewData();

                    foreach($a_data as $key_pattern => $pattern)
                    {
                        foreach($pattern as $key_field => $field_content)
                        {
                            if($field_content instanceof UploadedFile)
                            {
                                $web_path = $this->get('kernel')->getRootDir() . '/../web/uploads/scf/images/';
                                $newname = uniqid().'.'.$field_content->getClientOriginalExtension();

                                $field_content->move($web_path, $newname);

                                $field_content = $newname;
                            }

                            $edit_field   = new Field();
                            $field_exist = $e_field->findOneBy(['type' => $key_field, 'parent' => $idCollection, 'pattern' => $key_pattern]);

                            if($field_exist) $edit_field = $field_exist;

                            $edit_field->setPage($page);
                            $edit_field->setName('pattern_'.$key_field);
                            $edit_field->setType($key_field);
                            $edit_field->setIsNullable(0);

                            if($edit_field->getSlug() == null)
                                $edit_field->setSlug(uniqid());

                            if($field_content != null)
                                $edit_field->setContent($field_content);

                            $edit_field->setPosition(0);
                            $edit_field->setParent($idCollection);
                            $edit_field->setPattern($key_pattern);

                            $em->persist($edit_field);
                        }
                    }
                }
                else
                {
                    $upField = $e_field->findOneBy(['slug' => $f->getName()]);

                    $upField->setContent($f->getViewData());

                    $em->persist($upField);
                }
            }

            $em->flush();

            $this->addFlash('success', 'Les champs de la page ont bien été mis à jour');
            return $this->redirect($this->generateUrl('melodies_custom_fields_field_edit', ['id' => $page->getId()]));
        }

        return $this->render('MelodiesCustomFieldsBundle:Manage:fieldEdit.html.twig', [
            "page"   => $page,
            "form"   => $form->createView(),
            "fields" => $fields
        ]);
    }

    private function getTypeForm(Field $field, $form)
    {
        switch($field->getType())
        {
            case 'text':
                $form->add($field->getSlug(), $field->getType(), [
                    'label' => $field->getName(),
                    'data'  => $field->getContent()
                ]);
                break;
            case 'textarea':
                $form->add($field->getSlug(), $field->getType(), [
                    'label' => $field->getName(),
                    'data'  => $field->getContent(),
                ]);
                break;
            case 'ckeditor':
                $form->add($field->getSlug(), CKEditorType::class, [
                    'data'  => $field->getContent(),
                    'label' => $field->getName()
                ]);
                break;
            case 'integer':
                $form->add($field->getSlug(), 'integer', [
                    'label' => $field->getName()
                ]);
                break;
            case 'repeater':
                $em = $this->getDoctrine()->getManager();

                $collectionForm = $this->generatePattern($field->getId());
                $subFieldsExist = $em->getRepository( 'MelodiesCustomFieldsBundle:Field' )->getSubFieldsByPattern($field->getId());

                $form->add($field->getSlug(), CollectionType::class, [
                    'label'        => $field->getName(),
                    'required'     => false,
                    'type'         => $collectionForm,
                    'prototype'    => true,
                    'allow_add'    => true,
                    'allow_delete' => true,
                    'attr'         => [
                        'class'      => 'repeater_field',
                        'exist_data' => $subFieldsExist,
                    ],
                    'options'      => [ ]
                ]);
                break;
            case 'image':
                $form->add($field->getSlug(), FileType::class, array(
                    'label' => $field->getName(),
                    'image_path' => $field->getContent(),
                    'required' => false
                ));
                break;
            case 'file':
                $form->add($field->getSlug(), FileType::class, array(
                    'label' => $field->getName(),
                    'file_path' => $field->getContent(),
                    'required' => false
                ));
                break;
            default:
                $form->add($field->getSlug(), $field->getType(), [
                    'label' => $field->getName(),
                    'data' => $field->getContent()
                ]);

        }
    }

    public function generatePattern($field_id)
    {
        $em       = $this->getDoctrine()->getManager();
        $e_field  = $em->getRepository( 'MelodiesCustomFieldsBundle:Field' );

        $fields = $e_field->findBy(['parent' => $field_id, 'pattern' => 0]);

        $formBuilder = new PatternType($fields);

        return $formBuilder;
    }

    public function ajaxFieldAction(Request $request)
    {
        $data = $request->request->all();
        $action = $data['action'];
        $r_parent = $data['parent'];
        $r_fields = $data['fields'];
        $r_page = $data['page'];


        $form = $this->createForm(new FieldType());

        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['id' => $r_page]);
        $fields = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findBy(['page' => $page, 'pattern' => 0]);

        $choices = [
            'textarea' => "Textarea",
            'text' => "Text",
            'integer' => "Nombre",
            'url' => "Url",
            'choice' => "Checkbox",
            'ckeditor' => "Wiziwig",
            'image'   => "Image",
            'file' => "Fichier",
            'repeater' => "Répéteur de sections"
        ];

        if($action == "add")
        {
            $e_field = new Field();
            $e_field->setPage($page);
            $e_field->setPosition(0);
            $e_field->setParent($r_parent);
            $e_field->setSlug(uniqid());

            $em->persist($e_field);
            $em->flush();

            $fields = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findBy(['page' => $page, 'pattern' => 0]);
        }

        $this->getFieldsData($r_fields, $page);
        //$level = $data['level'];

        return $this->render('MelodiesCustomFieldsBundle:Ajax:field.html.twig', [
            "form" => $form->createView(),
            "fields" => $fields,
            "choices" => $choices
        ]);
    }

    private function getFieldsData($fields, Page $page)
    {
        $em = $this->getDoctrine()->getManager();
        $fields = json_decode(stripslashes($fields));

        if($fields == null) return;

        foreach($fields as $field)
        {
            $e_field = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findOneBy(['id' => $field->id]);

            $e_field->setName($field->name);
            $e_field->setType($field->type);
            $e_field->setIsNullable($field->isNullable);
            $e_field->setContent($field->content);
            $e_field->setPosition($field->position);

            $em->persist($e_field);
        }

        $em->flush();
    }

    public function ajaxFieldDeleteAction(Request $request)
    {
        $data = $request->request->all();
        $em   = $this->getDoctrine()->getManager();

        $id_field = $data['field'];

        $subfields = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findBy(['parent' => $id_field]);

        foreach($subfields as $subfield)
        {
            $em->remove($subfield);
        }

        $field = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findOneBy(['id' => $id_field]);

        $em->remove($field);
        $em->flush();


        return new JsonResponse(true);
    }

    public function ajaxPatternDeleteAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $slug = $data['slug']; // slug pattern
        $num = $data['num']; // num pattern

        $id_pattern = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findOneBy(['slug' => $slug]);

        if($id_pattern == null) return new JsonResponse(['error' => 'not found id pattern']);

        $patterns = $em->getRepository('MelodiesCustomFieldsBundle:Field')->findBy([
            'parent' => $id_pattern->getId(),
            'pattern' => $num
        ]);

        if($patterns == null) return new JsonResponse(['error' => 'not found pattern']);

        foreach($patterns as $pattern)
        {
            $em->remove($pattern);
            $em->flush();
        }

        return new JsonResponse(true);
    }
}
