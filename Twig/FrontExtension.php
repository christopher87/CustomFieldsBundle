<?php

namespace Melodies\CustomFieldsBundle\Twig;

class FrontExtension extends \Twig_Extension
{
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('mcf', [$this, 'mcfFunction'])
		];
	}

	public function mcfFunction($name, $subject, $default = null)
	{
		if(!isset($subject[$name])) return $default;


		$res = $subject[$name];

		return $res;
	}



	public function getName()
	{
		return 'mcf_front_extension';
	}
}