<?php

/**
 * Created by PhpStorm.
 * User: guillaume
 */
namespace Melodies\CustomFieldsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class MCFExtension extends \Twig_Extension
{
	protected $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('mcf_path_css', [$this, 'getPathCSSFunction']),
			new \Twig_SimpleFunction('mcf_path_js', [$this, 'getPathJsFunction'])
		];
	}

	/**
	 * Renders the css path.
	 *
	 * @return string Render css path
	 */
	public function getPathCSSFunction()
	{
		$cssPath = $this->container->getParameter('melodies.custom_fields.css_path');

		return $this->getAssetsHelper()->getUrl($cssPath);
	}

	/**
	 * Renders the js path.
	 *
	 * @return string Render js path
	 */
	public function getPathJsFunction()
	{
		$jsPath = $this->container->getParameter('melodies.custom_fields.js_path');

		return $this->getAssetsHelper()->getUrl($jsPath);
	}

	/**
	 * Fixes a path.
	 *
	 * @param string $path The path.
	 *
	 * @return string The fixed path.
	 */
	protected function fixPath($path)
	{
		if (($position = strpos($path, '?')) !== false) {
			return substr($path, 0, $position);
		}

		return $path;
	}

	/**
	 * Gets the assets helper.
	 *
	 * @return \Symfony\Component\Templating\Helper\CoreAssetsHelper The assets helper.
	 */
	protected function getAssetsHelper()
	{
		return $this->container->get('templating.helper.assets');
	}

	public function getName()
	{
		return 'mcf_extension';
	}
}